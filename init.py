#!/usr/bin/env python3
import telegram

# Telegram bot token and chat ID
bot_token = 'API_KEY'
chat_id = 'CHAT_ID'

def send_message(text):
    bot = telegram.Bot(token=bot_token)
    bot.send_message(chat_id=chat_id, text=text)

# Example usage
if __name__ == '__main__':
    send_message('jin Server rebooted')

